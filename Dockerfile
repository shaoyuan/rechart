FROM node:10.15.0
RUN npm update -g npm
RUN mkdir -p /react
ADD ./ ./react
WORKDIR /react
RUN npm -y init
RUN npm install

RUN chmod +x ./docker-entrypoint.sh

EXPOSE 3000

ENTRYPOINT ["./docker-entrypoint.sh"]