import React, { Component } from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ReferenceLine, ScatterChart, Scatter } from 'recharts';
import {info} from './1.json';

const data3 = info;


function demoOnClick(e) {
  alert(e.name);
}

const RenderNoShape = (props)=>{ 
 return null; 
}

class Chart extends Component {
  render() {
    return (
      <div className="Chart">
        <LineChart width={800} height={400} data={data3}>
          <Line type="monotone" dataKey="lines" stroke="#8884d8" dot={false}/>
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <ReferenceLine y={100} label="100 Lines" stroke="red" />
          <ReferenceLine x={7000} label="<== 7000 contracts" stroke="red" />
          <XAxis />
          <YAxis domain={[0,4000]}/>
        </LineChart>

        <ScatterChart width={800} height={400} margin={{top: 20, right: 20, bottom: 20, left: 20}}>
          <CartesianGrid />
          <YAxis dataKey={'score'} type="number" name='score' />
          <XAxis dataKey={'lines'} type="number" name='lines' domain={[0,4000]}/>
          <Scatter name='score' data={data3} fill='#8884d8' />
        </ScatterChart>
      </div>
    );
  }
}

export default Chart;
