import React, { Component } from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ReferenceLine, ScatterChart, Scatter } from 'recharts';
import {score} from './score_dapp.json';
import {tod} from './score_tod.json';
import {timestamp} from './score_timestamp.json';
import {reentrancy} from './score_reentrancy.json';
import {compare} from './compare.json';

const data6 = compare;
const data = score;
const data2 = tod;
const data4 = timestamp;
const data5 = reentrancy;

function scroe(data) {
  var i;
  for (i = 0; i < data.length; i++) {
    data[i].score -= 0;
  }
}

function limit(data) {
  var q;
  for (q = 0; q < data.length; q++) {
    if (data[q].score < 0) {
      data[q].score = 0;
    }
  }
}

limit(data2)
limit(data4)
limit(data5)

function demoOnClick(e) {
  alert(e.name);
}

const RenderNoShape = (props)=>{ 
 return null; 
}

class Chart extends Component {
  render() {
    return (
      <div className="Chart">
        <strong>
          Compare revised score with EVM score
        </strong>
        <LineChart width={1600} height={800} data={data6}>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="score" stroke="#ff0000" dot={false}/>
          <Line type="monotone" dataKey="evm_score" stroke="#993300" dot={false}/>
          <Line type="monotone" dataKey="Callstack Depth Attack Vulnerability" stroke="#ffff00" dot={false}/>
          <Line type="monotone" dataKey="Integer Overflow" stroke="#99ff33" dot={false}/>
          <Line type="monotone" dataKey="Integer Underflow" stroke="#33cc33" dot={false}/>
          <Line type="monotone" dataKey="Parity Multisig Bug 2" stroke="#00ff99" dot={false}/>
          <Line type="monotone" dataKey="Re-Entrancy Vulnerability" stroke="#33cccc" dot={false}/>
          <Line type="monotone" dataKey="Timestamp Dependency" stroke="#0099ff" dot={false}/>
          <Line type="monotone" dataKey="Transaction-Ordering Dependence (TOD)" stroke="#3366ff" dot={false}/>
          <Line type="monotone" dataKey="class_count" stroke="#9966ff" dot={false}/>
          <Line type="monotone" dataKey="delegatecall" stroke="#ff00ff" dot={false}/>
          <Line type="monotone" dataKey="forced-ether" stroke="#ff3399" dot={false}/>
          <Line type="monotone" dataKey="short-addr" stroke="#ff5050" dot={false}/>
          <Line type="monotone" dataKey="timestamp" stroke="#ff9933" dot={false}/>
          <Line type="monotone" dataKey="unchecked_math" stroke="#003300" dot={false}/>
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <XAxis />
          <YAxis />
        </LineChart>

        <strong>
          revised score w/o positive signs (adding score)
        </strong>
        <LineChart width={1600} height={800} data={data}>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="score" stroke="#ff0000" dot={false}/>
          <Line type="monotone" dataKey="evm_score" stroke="#993300" dot={false}/>
          <Line type="monotone" dataKey="Callstack Depth Attack Vulnerability" stroke="#ffff00" dot={false}/>
          <Line type="monotone" dataKey="Integer Overflow" stroke="#99ff33" dot={false}/>
          <Line type="monotone" dataKey="Integer Underflow" stroke="#33cc33" dot={false}/>
          <Line type="monotone" dataKey="Parity Multisig Bug 2" stroke="#00ff99" dot={false}/>
          <Line type="monotone" dataKey="Re-Entrancy Vulnerability" stroke="#33cccc" dot={false}/>
          <Line type="monotone" dataKey="Timestamp Dependency" stroke="#0099ff" dot={false}/>
          <Line type="monotone" dataKey="Transaction-Ordering Dependence (TOD)" stroke="#3366ff" dot={false}/>
          <Line type="monotone" dataKey="class_count" stroke="#9966ff" dot={false}/>
          <Line type="monotone" dataKey="delegatecall" stroke="#ff00ff" dot={false}/>
          <Line type="monotone" dataKey="forced-ether" stroke="#ff3399" dot={false}/>
          <Line type="monotone" dataKey="short-addr" stroke="#ff5050" dot={false}/>
          <Line type="monotone" dataKey="timestamp" stroke="#ff9933" dot={false}/>
          <Line type="monotone" dataKey="unchecked_math" stroke="#003300" dot={false}/>
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <XAxis />
          <YAxis />
        </LineChart>

        <strong>
          TOD
        </strong>
        <LineChart width={1600} height={800} data={data2}>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="score" stroke="#ff0000" dot={false}/>
          <Line type="monotone" dataKey="evm_score" stroke="#993300" dot={false}/>
          <Line type="monotone" dataKey="Callstack Depth Attack Vulnerability" stroke="#ffff00" dot={false}/>
          <Line type="monotone" dataKey="Integer Overflow" stroke="#99ff33" dot={false}/>
          <Line type="monotone" dataKey="Integer Underflow" stroke="#33cc33" dot={false}/>
          <Line type="monotone" dataKey="Parity Multisig Bug 2" stroke="#00ff99" dot={false}/>
          <Line type="monotone" dataKey="Re-Entrancy Vulnerability" stroke="#33cccc" dot={false}/>
          <Line type="monotone" dataKey="Timestamp Dependency" stroke="#0099ff" dot={false}/>
          <Line type="monotone" dataKey="Transaction-Ordering Dependence (TOD)" stroke="#3366ff" dot={false}/>
          <Line type="monotone" dataKey="class_count" stroke="#9966ff" dot={false}/>
          <Line type="monotone" dataKey="delegatecall" stroke="#ff00ff" dot={false}/>
          <Line type="monotone" dataKey="forced-ether" stroke="#ff3399" dot={false}/>
          <Line type="monotone" dataKey="short-addr" stroke="#ff5050" dot={false}/>
          <Line type="monotone" dataKey="timestamp" stroke="#ff9933" dot={false}/>
          <Line type="monotone" dataKey="unchecked_math" stroke="#003300" dot={false}/>
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <XAxis />
          <YAxis />
        </LineChart>

        <strong>
          timestamp Dependence
        </strong>
        <LineChart width={1600} height={800} data={data4}>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="score" stroke="#ff0000" dot={false}/>
          <Line type="monotone" dataKey="evm_score" stroke="#993300" dot={false}/>
          <Line type="monotone" dataKey="Callstack Depth Attack Vulnerability" stroke="#ffff00" dot={false}/>
          <Line type="monotone" dataKey="Integer Overflow" stroke="#99ff33" dot={false}/>
          <Line type="monotone" dataKey="Integer Underflow" stroke="#33cc33" dot={false}/>
          <Line type="monotone" dataKey="Parity Multisig Bug 2" stroke="#00ff99" dot={false}/>
          <Line type="monotone" dataKey="Re-Entrancy Vulnerability" stroke="#33cccc" dot={false}/>
          <Line type="monotone" dataKey="Timestamp Dependency" stroke="#0099ff" dot={false}/>
          <Line type="monotone" dataKey="Transaction-Ordering Dependence (TOD)" stroke="#3366ff" dot={false}/>
          <Line type="monotone" dataKey="class_count" stroke="#9966ff" dot={false}/>
          <Line type="monotone" dataKey="delegatecall" stroke="#ff00ff" dot={false}/>
          <Line type="monotone" dataKey="forced-ether" stroke="#ff3399" dot={false}/>
          <Line type="monotone" dataKey="short-addr" stroke="#ff5050" dot={false}/>
          <Line type="monotone" dataKey="timestamp" stroke="#ff9933" dot={false}/>
          <Line type="monotone" dataKey="unchecked_math" stroke="#003300" dot={false}/>
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <XAxis />
          <YAxis />
        </LineChart>

        <strong>
          Re-Entrancy
        </strong>
        <LineChart width={1600} height={800} data={data5}>
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="score" stroke="#ff0000" dot={false}/>
          <Line type="monotone" dataKey="evm_score" stroke="#993300" dot={false}/>
          <Line type="monotone" dataKey="Callstack Depth Attack Vulnerability" stroke="#ffff00" dot={false}/>
          <Line type="monotone" dataKey="Integer Overflow" stroke="#99ff33" dot={false}/>
          <Line type="monotone" dataKey="Integer Underflow" stroke="#33cc33" dot={false}/>
          <Line type="monotone" dataKey="Parity Multisig Bug 2" stroke="#00ff99" dot={false}/>
          <Line type="monotone" dataKey="Re-Entrancy Vulnerability" stroke="#33cccc" dot={false}/>
          <Line type="monotone" dataKey="Timestamp Dependency" stroke="#0099ff" dot={false}/>
          <Line type="monotone" dataKey="Transaction-Ordering Dependence (TOD)" stroke="#3366ff" dot={false}/>
          <Line type="monotone" dataKey="class_count" stroke="#9966ff" dot={false}/>
          <Line type="monotone" dataKey="delegatecall" stroke="#ff00ff" dot={false}/>
          <Line type="monotone" dataKey="forced-ether" stroke="#ff3399" dot={false}/>
          <Line type="monotone" dataKey="short-addr" stroke="#ff5050" dot={false}/>
          <Line type="monotone" dataKey="timestamp" stroke="#ff9933" dot={false}/>
          <Line type="monotone" dataKey="unchecked_math" stroke="#003300" dot={false}/>
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <XAxis />
          <YAxis />
        </LineChart>
      </div>
    );
  }
}

export default Chart;
