import React, { Component } from 'react';
import { CartesianGrid, XAxis, YAxis, BarChart, Bar, Tooltip, Legend } from 'recharts';

const data3 = [
  {'name' : 'Callstack Depth Attack Vulnerability', 'False': 42693, 'True': 775},
  {'name' : 'Integer Overflow', 'False': 8748, 'True': 34720},
  {'name' : 'Integer Underflow' , 'False': 17599, 'True': 25869},
  {'name' : 'Parity Multis : ig Bug 2' , 'False': 43465, 'True': 3},
  {'name' : 'Re-Entrancy Vulnerability' , 'False': 43125, 'True': 343},
  {'name' : 'Timestamp Dependency' , 'False': 41970, 'True': 1498},
  {'name' : 'Transaction-Ordering Dependence (TOD)' , 'False': 38765, 'True': 4703}
]

const data4 = [
  {"score": "0-4", "count": 369},
  {"score": "5-9", "count": 2},
  {"score": "10-14", "count": 8},
  {"score": "15-19", "count": 21},
  {"score": "20-24", "count": 4},
  {"score": "25-29", "count": 11},
  {"score": "30-34", "count": 29},
  {"score": "35-39", "count": 47},
  {"score": "40-44", "count": 50},
  {"score": "45-49", "count": 81},
  {"score": "50-54", "count": 129},
  {"score": "55-59", "count": 185},
  {"score": "60-64", "count": 448},
  {"score": "65-69", "count": 518},
  {"score": "70-74", "count": 671},
  {"score": "75-79", "count": 1255},
  {"score": "80-84", "count": 1583},
  {"score": "85-89", "count": 2641},
  {"score": "90-94", "count": 6611},
  {"score": "95-99", "count": 24517},
  {"score": "100", "count": 4291}
]

function demoOnClick(e) {
  alert(e.name);
}

class Chart extends Component {
  render() {
    return (
      <div className="Chart">
        <BarChart width={800} height={600} data={data3}
            margin={{top: 20, right: 30, left: 20, bottom: 5}}>
         <CartesianGrid strokeDasharray="3 3"/>
         <XAxis dataKey="name"/>
         <YAxis/>
         <Tooltip/>
         <Legend />
         <Bar dataKey="True" stackId="a" fill="#8884d8" onClick={demoOnClick} />
         <Bar dataKey="False" stackId="a" fill="#D0D3D4" onClick={demoOnClick} />
        </BarChart>

        <BarChart width={800} height={600} data={data4}
            margin={{top: 20, right: 30, left: 20, bottom: 5}}>
         <CartesianGrid strokeDasharray="3 3"/>
         <XAxis dataKey="score"/>
         <YAxis/>
         <Tooltip/>
         <Legend />
         <Bar dataKey="count" stackId="a" fill="#8884d8" label={{ position: 'top' }} onClick={demoOnClick} />
        </BarChart>
      </div>
    );
  }
}

export default Chart;
