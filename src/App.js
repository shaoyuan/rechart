import React, { Component } from 'react';
import { Router, Route, BrowserRouter, Switch } from 'react-router-dom';

import Root from './Chart';
import Chart from './Chart3';
import List from './Address';
import './App.css'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Root} />
          <Route path='/chart' component={Chart} />
          <Route path='/address' component={List} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
